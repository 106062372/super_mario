const { ccclass, property } = cc._decorator;

@ccclass
export  class Enemy extends cc.Component {

    private rebornPos = null;

    private isDead = true;

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start() {
        this.rebornPos = this.node.position;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        this.isDead = false;
    }

    update(dt) {
        if (this.isDead) {
            //this.resetPos();
            this.isDead = false;
        }
    }

    public resetPos() {
        this.node.position.x = 366;
        this.node.position.y = 95
        this.node.scaleX = 2;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
    }

    public todeath() {
        this.anim.play('mushroom_die'); 
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
        this.scheduleOnce(function () {
            this.node.destroy();
        }, 0.4);
               
    }

    onBeginContact(contact, self, other) {
        if (other.node.name == "left_bound_1") {
            this.node.scaleX = -2;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
        } else if (other.node.name == "right_bound_1") {
            this.node.scaleX = 2;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        } else if (other.node.name == "Player") {
            if (this.node.y + 20 < other.node.y) {
                cc.log("mushroom die");
                //this.node.destroy();
                this.todeath();
            } else {
                cc.log("mushroom attack");
                this.isDead = true;
            }
            //this.isDead = true;

        }

    }


}
