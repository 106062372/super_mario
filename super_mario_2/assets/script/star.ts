// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class coin extends cc.Component {


    @property({type:cc.AudioClip})
    audioClips: cc.AudioClip[] = [];

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    /*moving(){
        let up = cc.moveBy(0.5, 0, 68);
        let down = cc.moveBy(0.5, 0, -68);
        up.easing(cc.easeInOut(2));
        down.easing(cc.easeInOut(2));
        let action = cc.repeatForever(
            cc.sequence(up, down));

        this.node.runAction(action);
    }*/

    onBeginContact(contact, self, other) {
        if (other.node.name == "Player") {
            cc.audioEngine.playEffect(this.audioClips[0], false);
            this.node.destroy();
        } 

    }

    // update (dt) {}
}
