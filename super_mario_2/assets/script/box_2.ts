// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html


const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {



    @property(cc.Label)
    label: cc.Label = null;

    @property({type:cc.AudioClip})
    audioClips: cc.AudioClip[] = [];

    @property(cc.Node)
    private star: cc.Node = null;

    private anim = null; //this will use to get animation component

    private box_state:boolean = true;




    onLoad() {
        this.anim = this.getComponent(cc.Animation);
    }

    start () {

    }

    onBeginContact(contact, self, other) {
        if (other.node.name == "Player") {
            if(this.node.position.y - other.node.position.y > 38 && this.box_state == true){                
                cc.audioEngine.playEffect(this.audioClips[0], false);
                this.anim.play('box_die');
                this.box_state = false;
                //this.star.y = 350;


                let up = cc.moveBy(0.5, 0, 80);
                let down = cc.moveBy(0.5, 0, -80);
                up.easing(cc.easeInOut(2));
                down.easing(cc.easeInOut(2));
                let action = cc.repeatForever(
                    cc.sequence(up, down));
        
                this.star.runAction(action);
            }
        }
    }

    // update (dt) {}
}
