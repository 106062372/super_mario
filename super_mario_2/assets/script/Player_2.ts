import { Enemy } from "./Enemy";

const { ccclass, property } = cc._decorator;

@ccclass
export class Player extends cc.Component {

    @property({ type: cc.AudioClip })
    audioClips: cc.AudioClip[] = [];


    @property(Enemy)
    enemy: Enemy = null;

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;

    @property(cc.Node)
    private mainCamera: cc.Node = null;



    @property(cc.Node)
    life_5: cc.Node = null;
    @property(cc.Node)
    life_4: cc.Node = null;
    @property(cc.Node)
    life_3: cc.Node = null;
    @property(cc.Node)
    life_2: cc.Node = null;
    @property(cc.Node)
    life_1: cc.Node = null;



    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    private playerSpeed: number = 0;

    private zDown: boolean = false; // key for player to go left

    private xDown: boolean = false; // key for player to go right

    private jDown: boolean = false; // key for player to shoot

    private kDown: boolean = false; // key for player to jump

    private isDead: boolean = false;

    private onGround: boolean = false;

    private life: number = 5;

    private flag: number = 1;

    private star: number = 0;

    private sound_effect: number = 1;



    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
        cc.audioEngine.playEffect(this.audioClips[0], true);
    }

    update(dt) {
        this.playerMovement(dt);
        this.testdeath();
        this.mainCamera.x = this.node.x - 100;
    }

    onKeyDown(event) {
        if (event.keyCode == cc.macro.KEY.z) {
            this.zDown = true;
            this.xDown = false;
        } else if (event.keyCode == cc.macro.KEY.x) {
            this.xDown = true;
            this.zDown = false;
        } else if (event.keyCode == cc.macro.KEY.k) {
            this.kDown = true;
        } else if (event.keyCode == cc.macro.KEY.j) {
            this.jDown = true;
            this.createBullet();
        }
    }

    onKeyUp(event) {
        if (event.keyCode == cc.macro.KEY.z)
            this.zDown = false;
        else if (event.keyCode == cc.macro.KEY.x)
            this.xDown = false;
        else if (event.keyCode == cc.macro.KEY.j)
            this.jDown = false;
        else if (event.keyCode == cc.macro.KEY.k)
            this.kDown = false;
    }


    private playerMovement(dt) {
        this.playerSpeed = 0;
        if (this.isDead) {
            cc.audioEngine.playEffect(this.audioClips[3], false);
            if (this.life == 5) {
                this.life_5.destroy();
                this.life--;
            } else if (this.life == 4) {
                this.life_4.destroy();
                this.life--;
            } else if (this.life == 3) {
                this.life_3.destroy();
                this.life--;
            } else if (this.life == 2) {
                this.life_2.destroy();
                this.life--;
            } else if (this.life == 1) {
                this.life_1.destroy();
                this.life--;
            }            


            if(this.life == 0){
                cc.director.loadScene("game_over");
            }
            
            /*this.scheduleOnce(function () {         
                cc.audioEngine.playEffect(this.audioClips[0], false);
            }, 3);*/
            //cc.audioEngine.stopEffect(0);
            //cc.director.loadScene("scene1");
            
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            this.node.position = cc.v2(20, 200);
            this.isDead = false;
            this.enemy.resetPos(); 


            return;
        }

        if (this.kDown && this.onGround) {
            this.jump();
            this.animateState = this.anim.play('jump');
        } else {
            if (this.zDown) {
                this.playerSpeed = -300;
                this.node.scaleX = -2;
                if (this.animateState == null || this.animateState.name != 'move' && this.onGround == true) {
                    this.animateState = this.anim.play('move');
                }
            }
            else if (this.xDown) {
                this.playerSpeed = 300;
                this.node.scaleX = 2;
                if (this.animateState == null || this.animateState.name != 'move' && this.onGround == true) {
                    this.animateState = this.anim.play('move');
                }
            }
            else {
                if (this.onGround == true) {
                    this.animateState = this.anim.play('idle');
                }
            }
        }

        this.node.x += this.playerSpeed * dt;


    }

    private jump() {
        cc.audioEngine.playEffect(this.audioClips[1], false);
        this.onGround = false;
        // Method I: Apply Force to rigidbody
        if (this.getComponent(cc.RigidBody).linearVelocity.y == 0) {//防止擦到邊邊會飛起來
            this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0, 150000), true);
        }
    }

    private createBullet() {
        let bullet = cc.instantiate(this.bulletPrefab);
        bullet.getComponent('Bullet_ans').init(this.node);
    }



    private testdeath() {
        if (this.node.y < 0) {
            this.isDead = true;
            cc.log("y out of bounds");
        }
        /*if(this.node.x < 0){
            this.isDead = true;
            cc.log("x out of bounds");
        }*/
    }




    onBeginContact(contact, self, other) {
        if (other.node.name == "background") {
            console.log("mario hits the background");
            this.onGround = true;
        } else if (other.node.name == "box") {
            console.log("mario hits the box");
            this.onGround = true;
        } else if (other.node.name == "enemy") {
            if (this.node.y - 20 > other.node.y) {
                cc.audioEngine.playEffect(this.audioClips[2], false);
                console.log("mario attack");
            } else {
                console.log(this.node.y);
                console.log(other.node.y);
                console.log("mario die");

                if(this.star==0)
                    this.isDead = true;
            }
            //this.isDead = true;
        } else if (other.node.name == "sharp") {            
            if(this.star==0)
                this.isDead = true;
        } else if (other.node.name == "flag") {
            if(this.sound_effect == 1){
                cc.audioEngine.stopAll();
                cc.audioEngine.playEffect(this.audioClips[4], false);
                this.sound_effect = 0;
            }
            
            this.scheduleOnce(function () {         
                cc.audioEngine.stopAll();
                cc.director.loadScene("menu");
            }, 5);

        } else if (other.node.name == "block") {
            console.log(this.node.y);
            console.log(other.node.y);
            if (this.node.y - other.node.y < 100) {
                contact.disabled = true;
            } else {
                this.onGround = true;
                console.log("mario hits the block");
            }
        } else if (other.node.name == "flower") {
            console.log("player:",this.node.y);
            console.log("flower",other.node.y);
            if (other.node.y + 47 < this.node.y) {
                
            }else{
                if(this.star==0)
                    this.isDead = true;
            }
        }else if(other.node.name == "star"){
            this.star = 1;
            cc.audioEngine.stopAll();
            cc.audioEngine.playEffect(this.audioClips[5], false);
            this.scheduleOnce(function () {         
                cc.audioEngine.stopAll();       
                cc.audioEngine.playEffect(this.audioClips[0], false);                
                this.star = 0;
            }, 10);
        }

    }

}