const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    @property(cc.EditBox)
    email: cc.EditBox = null;

    @property(cc.EditBox)
    password: cc.EditBox = null;

    @property({ type: cc.AudioClip })
    audioClips: cc.AudioClip[] = [];





    onLoad() {
        //let email_ = this.email.string;
        //let password_ = this.password.string;
        cc.audioEngine.playEffect(this.audioClips[0], false);

    }


    signup() {
        let email_ = this.email.string;
        let password_ = this.password.string;
        firebase.auth().createUserWithEmailAndPassword(this.email.string, this.password.string)
            .then(function () {
                alert("signup success")
                firebase.database().ref('users/users').push({
                    "user_email": email_,
                    "user_password": password_
                })
            })
            .catch(function (error) {
                alert(error.message)
            });
    }

    start() {

    }

    update(dt) {

    }

}
