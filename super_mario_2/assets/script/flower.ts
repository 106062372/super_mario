const { ccclass, property } = cc._decorator;

@ccclass
export class Enemy extends cc.Component {
    
    @property({ type: cc.AudioClip })
    audioClips: cc.AudioClip[] = [];

    private rebornPos = null;

    private isDead = true;

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start() {
        this.rebornPos = this.node.position;

        let up = cc.moveBy(3, 0, 68);
        let down = cc.moveBy(3, 0, -68);
        up.easing(cc.easeInOut(2));
        down.easing(cc.easeInOut(2));
        let action = cc.repeatForever(
            cc.sequence(up, down));

        this.node.runAction(action);

        //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        this.isDead = false;
    }

    update(dt) {
        if (this.isDead) {
            //this.resetPos();
            this.isDead = false;
        }
    }



    public todeath() {
        this.anim.play('mushroom_die');
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
        this.scheduleOnce(function () {
            this.node.destroy();
        }, 0.4);
    }

    onBeginContact(contact, self, other) {
        if (other.node.name == "Player") {
            if (this.node.y + 47 < other.node.y) {
                cc.log("flower die");
                cc.audioEngine.playEffect(this.audioClips[0], false);
                this.node.destroy();
            } else {
                cc.log("flower attack");
                this.isDead = true;
            }
            //this.isDead = true;

        }

    }


}
