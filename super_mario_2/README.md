# Software Studio 2020 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description

# Basic Components Description : 
1. World map : 主要有兩個map，我將關卡難度設定的比較低，較利於遊玩，demo時也比較好操作，一關的目的地為最後的旗標。
2. Player : 人物（馬力歐）可進行走跳，當採在敵人（蘑菇、花）的頭上時可以殺掉敵人，但如果沒有在上面的話會是人物死掉。一關總共有五條命，被敵人殺掉、掉出邊界、採到尖刺皆會死去，少一條命，並在起點重生。
3. Enemies : 總共有三種敵人：蘑菇、花和夾子。蘑菇會筆直的前進，花會在紅色管子中冒出來。當敵人被馬力歐踩的時候就會死亡。還有夾子會在地板上，如果採到的話就會死亡，只能用跳過去的或者是吃到星星。
4. Question Blocks : 分為兩種Question Block。第一種為撞了會有錢，第二種為撞了會有星星可以吃，而猩猩可以無敵。當撞過方塊以後就不能再撞了。
5. Animations : 各角色的動畫如下
馬力歐：走路、跳、停滯
蘑菇：左右移動
花：上下移動、開口閉合
星星：上下移動
夾子：開口閉合
6. Sound effects : 各角色的音效如下
場景：第一關背景音樂、第二關背景音樂、登入和選擇關卡背景音樂、通關音樂、命全部用完音樂
馬力歐：跳、死亡、踩蘑菇、踩花、吃到星星後音樂
7. UI : 我原本全都有做，但在嘗試生命將生命和得分firebase時一直無法成功，想用回最初的code還沒辦法，無奈之下只好將現階段可以使用的code交出。只剩下時間功能和生命功能(local)。![](https://i.imgur.com/v90fSpD.png)
使用firebase證明，圖中為signup後創立的玩家資料。


# Bonus Functions Description : 
1. 星星 : 吃了可以變成無敵，但掉出場外還是一樣會死。
2. one way collision : 某些block是只有one way collision，增加可玩性。
3. 角色移動方式 : 花和星星的移動方式是使用ease緩動動作。
